package com.caveofprogramming.designpatterns.demo1.controller;

import java.sql.SQLException;

import com.caveofprogramming.designpatterns.demo1.model.Model;
import com.caveofprogramming.designpatterns.demo1.model.Person;
import com.caveofprogramming.designpatterns.demo1.model.PersonDAO;
import com.caveofprogramming.designpatterns.demo1.view.CreateUserEvent;
import com.caveofprogramming.designpatterns.demo1.view.CreateUserListener;
import com.caveofprogramming.designpatterns.demo1.view.View;

public class Controller implements CreateUserListener {

	private Model model;
	private View view;
	private PersonDAO personDAO = new PersonDAO();

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
	}

	@Override
	public void userCreated(CreateUserEvent event) {
		String name = event.getName();
		String password = event.getPassword();
		System.out.println("Login event received: " + name + " " + password);
		try {
			personDAO.addPerson(new Person(name, password));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
