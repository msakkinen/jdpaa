package com.caveofprogramming.designpatterns.demo1.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	private static Database instance = new Database();

	private Connection con;

	private Database() {

	}

	public static Database getInstance() {
		return instance;
	}

	public void connect() throws Exception {
		if (con != null)
			return;

		try {
			// TODO: replace path with your current path
			String url = "jdbc:sqlite:/home/mms/codes/jdpaa/DesignPatterns11/db/database.db";
			con = DriverManager.getConnection(url);
			System.out.println("Connection established");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("see the error log");
		}
	}

	public void disconnect() {
		if (con != null) {
			try {
				con.close();
				System.out.println("Connection closed");
			} catch (SQLException e) {
				System.out.println("Can't close connection");
			}
		}

		con = null;
	}
}
