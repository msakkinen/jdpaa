/*
 * Imagine that this class requires lots of configuration
 * before use, eg. via constructor parameter
 */
public class Cat extends AbstractAnimal implements Animal {

	@Override
	public void speak() {
		System.out.println("Meouw!");
	}

}
